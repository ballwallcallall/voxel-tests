﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class AirBlock : Block {


    public AirBlock(): base()
     {
 
     }

     public override MeshData Blockdata
         (Chunk chunk, int x, int y, int z, MeshData meshData)
     {
         return meshData;
     }
 
     public override bool IsSolid(Block.Direction direction)
     {
         return false;
     }
}
