﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class GrassBlock : Block {

    [Serializable]
    public struct custom_Vector2
    {
        public int x, y;

        public custom_Vector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

    }

    public custom_Vector2 uv_side = new custom_Vector2(3, 0);
    public custom_Vector2 uv_top = new custom_Vector2(2, 0);
    public custom_Vector2 uv_bottom = new custom_Vector2(1, 0);



    public GrassBlock()
        : base()
     {
 
     }

     public override Tile TexturePosition(Direction direction)
     {
         Tile tile = new Tile();

         switch (direction)
         {
             case Direction.up:
                 tile.x = uv_top.x;
                 tile.y = uv_top.y;
                 return tile;
             case Direction.down:
                 tile.x = uv_bottom.x;
                 tile.y = uv_bottom.y;
                 return tile;
         }

         tile.x = uv_side.x;
         tile.y = uv_side.y;

         return tile;
     }
}
